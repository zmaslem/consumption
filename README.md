This is the backend application which purpose is to provide the user energy consumption data
for a given dates range within last two years.

Tech-stack:
Java 8, spring-boot, lombok, jackson, aspect, caffeine, restassured, junit

How to start:
1) clone the repository
2) mvn clean install  - to make sure all the tests pass
3) mvn spring-boot:run OR java -jar target/consumption-0.0.1-SNAPSHOT.jar

The application runs on http://localhost:8080. 
There is only one rest endpoint available: http://localhost:8080/report and it accepts two path arguments
1- startDate
2- endDate
both in format 'MM-dd-yyyy'

The '/report' endpoint returns the data in application/json format that contains the userdata object 
with all important information like: list of energy consumption per day and total energy consumption
in given range of dates.

Application spring cache manager (caffeine implementation) for caching data with given key. The key consists of DatePeriod 
object which basically is the combination of startDate and endData, both as LocalDate objects.
Thus application asks finestmedia servers for data only if cache does not contain the UserData object
of given key (DatePeriod). Cache max size is set to 500, and objects are evicted after 10 minutes. These values are 
contractual and should be set to proper values only after analyzing statistics and  measuring usage of 
application.

Have fun!
