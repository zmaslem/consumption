package com.kawrobel.consumption;

import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.entity.report.*;
import org.assertj.core.util.Lists;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;

public class SampleData {
    public static DatePeriod datePeriod = DatePeriod.between(LocalDate.now().minusDays(1), LocalDate.now());

    public static EnergyReport prepareEnergyReport() {
        EnergyReport energyReport = new EnergyReport();

        AccountTimeSeries accountTimeSeries = new AccountTimeSeries();
        accountTimeSeries.setAccountingPoint("testingPoint");

        ConsumptionHistory consumptionHistory = new ConsumptionHistory();

        HourConsumption hourConsumption1 = new HourConsumption();
        hourConsumption1.setTs(ZonedDateTime.now());
        hourConsumption1.setValue(BigDecimal.valueOf(2));
        HourConsumption hourConsumption2 = new HourConsumption();
        hourConsumption2.setTs(ZonedDateTime.now().minusHours(2));
        hourConsumption2.setValue(BigDecimal.valueOf(2.2));
        HourConsumption hourConsumption3 = new HourConsumption();
        hourConsumption3.setTs(ZonedDateTime.now().minusDays(2));
        hourConsumption3.setValue(BigDecimal.valueOf(2.4));

        consumptionHistory.setHourConsumption(Lists.newArrayList(hourConsumption1, hourConsumption2, hourConsumption3));

        accountTimeSeries.setConsumptionHistory(consumptionHistory);

        accountTimeSeries.setMeasurementUnit(MeasurementUnit.KWH);

        energyReport.setAccountTimeSeries(accountTimeSeries);
        energyReport.setDocumentIdentification("ID");
        energyReport.setDocumentDateTime(ZonedDateTime.now());
        return energyReport;
    }
}
