package com.kawrobel.consumption.boundary;

import com.kawrobel.consumption.ConsumptionApplicationTests;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.jayway.restassured.RestAssured.given;

public class UserUsageDataControllerITest extends ConsumptionApplicationTests {
    private static final String CONTENT = "Content-Type";
    private static final String JSON = "application/json";
    private static final String ENDPOINT = "/report";
    private static final String DATE_FORMAT = "MM-dd-yyyy";

    @Test
    public void shouldReturnOK() {
        String start = LocalDate.now().minusDays(2).format(DateTimeFormatter.ofPattern(DATE_FORMAT));
        String end = LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern(DATE_FORMAT));

        given().log().all()
                .header(CONTENT, JSON)
                .port(port)
                .when()
                .get(ENDPOINT + "/" + start + "/" + end)
                .then()
                .statusCode(HttpStatus.OK.value());
    }

}