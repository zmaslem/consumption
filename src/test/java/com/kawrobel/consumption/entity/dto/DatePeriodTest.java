package com.kawrobel.consumption.entity.dto;

import com.kawrobel.consumption.exceptions.DataValidationException;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DatePeriodTest {

    @Test
    public void shouldBeEqual() {

        LocalDate one = LocalDate.of(2018, 1, 1);
        LocalDate two = LocalDate.of(2018, 2, 1);
        LocalDate three = LocalDate.of(2018, 1, 1);
        LocalDate four = LocalDate.of(2018, 2, 1);
        DatePeriod first = new DatePeriod(one, two);
        DatePeriod second = new DatePeriod(three, four);

        assertEquals(first, second);
    }

    @Test
    public void shouldNotBeEqual() {
        LocalDate one = LocalDate.of(2018, 1, 1);
        LocalDate two = LocalDate.of(2018, 2, 1);
        LocalDate three = LocalDate.of(2018, 1, 1);
        LocalDate four = LocalDate.of(2018, 3, 1);
        DatePeriod first = new DatePeriod(one, two);
        DatePeriod second = new DatePeriod(three, four);

        assertNotEquals(first, second);
    }

    @Test
    public void shouldNotBeEqualWithNull() {
        LocalDate one = LocalDate.of(2018, 1, 1);
        LocalDate two = LocalDate.of(2018, 2, 1);
        DatePeriod first = new DatePeriod(one, two);

        assertNotEquals(first, null);
    }

    @Test(expected = DataValidationException.class)
    public void shouldNotAcceptNullInConstructorForEnd() {
        LocalDate three = LocalDate.of(2018, 1, 1);
        LocalDate four = null;
        new DatePeriod(three, four);
    }

    @Test(expected = DataValidationException.class)
    public void shouldNotAcceptNullInConstructorForStart() {
        LocalDate three = LocalDate.of(2018, 1, 1);
        LocalDate four = null;
        new DatePeriod(four, three);
    }

    @Test(expected = DataValidationException.class)
    public void shouldNotAcceptNullInBetweenForEnd() {
        LocalDate three = LocalDate.of(2018, 1, 1);
        LocalDate four = null;
        DatePeriod.between(three, four);
    }

    @Test(expected = DataValidationException.class)
    public void shouldNotAcceptNullInBetweenForStart() {
        LocalDate three = LocalDate.of(2018, 1, 1);
        LocalDate four = null;
        DatePeriod.between(four, three);
    }

    @Test(expected = DataValidationException.class)
    public void shouldNotAcceptStartAfterEnd() {
        LocalDate one = LocalDate.of(2018, 1, 1);
        LocalDate two = LocalDate.of(2018, 2, 1);
        DatePeriod.between(two, one);
    }
}