package com.kawrobel.consumption.entity.converter;

import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.entity.dto.UserData;
import com.kawrobel.consumption.entity.report.EnergyReport;
import com.kawrobel.consumption.exceptions.NoDataAvailableException;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.kawrobel.consumption.SampleData.prepareEnergyReport;
import static org.junit.Assert.assertEquals;

public class EnergyReportToUserDataConverterTest {

    @Test
    public void shouldConvertEnergyReportToUserDataObject() {
        //given
        DatePeriod datePeriod = DatePeriod.between(LocalDate.now().minusDays(1), LocalDate.now().plusDays(1));
        EnergyReport energyReport = prepareEnergyReport();
        //when
        UserData userData = EnergyReportToUserDataConverter.convertFromEnergyReport(energyReport, datePeriod);
        //then
        assertEquals("Should contain two point", 1, userData.getDayConsumption().size());
        assertEquals("The total consumption should be 4.2", BigDecimal.valueOf(4.2), userData.getTotal());
    }

    @Test(expected = NoDataAvailableException.class)
    public void shouldThrowNoDataAvailableWhenEnergyReportDoesNotContainDatePeriodData() {
        //given
        DatePeriod datePeriod = DatePeriod.between(LocalDate.now().minusDays(4), LocalDate.now().minusDays(3));
        EnergyReport energyReport = prepareEnergyReport();
        //when
        EnergyReportToUserDataConverter.convertFromEnergyReport(energyReport, datePeriod);
        //then expect exception
    }

}