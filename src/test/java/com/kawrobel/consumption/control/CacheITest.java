package com.kawrobel.consumption.control;

import com.kawrobel.consumption.SampleData;
import com.kawrobel.consumption.entity.converter.EnergyReportToUserDataConverter;
import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.entity.dto.UserData;
import com.kawrobel.consumption.entity.report.EnergyReport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.TestCase.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class CacheITest {

    @Autowired
    CacheManager cacheManager;

    @Autowired
    private DataFetcher dataFetcher;

    @Configuration
    @EnableCaching
    public static class TestConfig {
        @Bean
        public CacheManager cacheManager() {
            return new CaffeineCacheManager("userData");
        }

        @Bean
        public DataFetcher dataFetcher() {
            return new DataFetcher() {
                @Override
                @CachePut(value = "userData", key = "#datePeriod")
                public UserData loadData(final DatePeriod datePeriod) {
                    EnergyReport energyReport = SampleData.prepareEnergyReport();
                    return EnergyReportToUserDataConverter.convertFromEnergyReport(energyReport, datePeriod);
                }
            };
        }
    }

    @Test
    public void cacheShouldNotBeEmptyAfterFirstCall() {
        //given prepared cache
        //when
        dataFetcher.fetchData(SampleData.datePeriod);
        //then
        assertNotNull(cacheManager.getCache("userData").get(SampleData.datePeriod));
    }
}
