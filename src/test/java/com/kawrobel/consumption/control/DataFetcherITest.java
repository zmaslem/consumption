package com.kawrobel.consumption.control;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.kawrobel.consumption.ConsumptionApplicationTests;
import com.kawrobel.consumption.entity.converter.EnergyReportToUserDataConverter;
import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.entity.dto.UserData;
import com.kawrobel.consumption.entity.report.EnergyReport;
import com.kawrobel.consumption.exceptions.DataValidationException;
import com.kawrobel.consumption.exceptions.NoDataAvailableException;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
public class DataFetcherITest extends ConsumptionApplicationTests {

    private String TEST_DATA_DIR;
    private AtomicInteger cacheHitCount = new AtomicInteger();

    @Autowired
    private ObjectMapper objectMapper;

    private DataFetcher CUT = new DataFetcher() {
        @Override
        public UserData loadData(final DatePeriod datePeriod) {
            cacheHitCount.incrementAndGet();
            objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            EnergyReport energyReport = new EnergyReport();
            try {
                File xmlFile = new ClassPathResource(TEST_DATA_DIR).getFile();
                energyReport = objectMapper.readValue(xmlFile, EnergyReport.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return EnergyReportToUserDataConverter.convertFromEnergyReport(energyReport, datePeriod);
        }

        @Override
        public UserData fetchData(final DatePeriod datePeriod) {
            return loadData(datePeriod);
        }
    };

    @After
    public void after() {
        cacheHitCount = new AtomicInteger();
    }

    @Test
    public void shouldProvideData() {
        //given
        TEST_DATA_DIR = "testdata.xml";
        BigDecimal expected = BigDecimal.valueOf(6392.39);
        LocalDate start = LocalDate.of(2017, 8, 4);
        LocalDate end = LocalDate.of(2018, 8, 5);
        DatePeriod datePeriod = DatePeriod.between(start, end);
        //when
        UserData userData = CUT.fetchData(datePeriod);
        //then
        assertNotNull(userData);
        //plus one because end date included, might be covered with manipulating user's input to include "today"
        assertEquals(DAYS.between(start, end) + 1, userData.getDayConsumption().size());
        assertEquals(expected, userData.getTotal());
    }

    @Test(expected = DataValidationException.class)
    public void shouldThrowValidationException() {
        //given
        LocalDate start = LocalDate.of(2017, 8, 4);
        LocalDate end = LocalDate.of(2018, 8, 5);
        DatePeriod datePeriod = DatePeriod.between(end, start);
        //when
        CUT.fetchData(datePeriod);
        //then exception
    }

    @Test(expected = NoDataAvailableException.class)
    public void shouldThrowNoDataAvailableWhenNoDataPresent() {
        //given
        TEST_DATA_DIR = "smalldata.xml";
        LocalDate start = LocalDate.of(2017, 8, 4);
        LocalDate end = LocalDate.of(2018, 8, 5);
        DatePeriod datePeriod = DatePeriod.between(start, end);
        //when
        UserData userData = CUT.fetchData(datePeriod);
        //then
        assertNotNull(userData);
    }
}