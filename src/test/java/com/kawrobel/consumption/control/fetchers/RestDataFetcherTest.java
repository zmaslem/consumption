package com.kawrobel.consumption.control.fetchers;

import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.entity.dto.UserData;
import com.kawrobel.consumption.exceptions.RestCallException;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class RestDataFetcherTest {

    @Mock
    private RestDataFetcher CUT = Mockito.mock(RestDataFetcher.class);

    @Test
    public void shouldReturnUserDataObject() throws RestCallException {
        //given
        LocalDate start = LocalDate.of(2017, 8, 4);
        LocalDate end = LocalDate.of(2018, 8, 5);
        DatePeriod datePeriod = DatePeriod.between(start, end);
        when(CUT.loadData(datePeriod)).thenReturn(new UserData(Lists.emptyList(), datePeriod, BigDecimal.ZERO));
        //when
        UserData userData = CUT.loadData(datePeriod);
        //then
        assertNotNull(userData);
    }

    @Test
    public void shouldProvideUserDataFromRestEndpoint() {
        //given
        LocalDate start = LocalDate.of(2017, 8, 4);
        LocalDate end = LocalDate.of(2018, 8, 5);
        DatePeriod datePeriod = DatePeriod.between(start, end);
        when(CUT.fetchData(datePeriod)).thenReturn(new UserData(Lists.emptyList(), datePeriod, BigDecimal.ZERO));
        //when
        UserData userData = CUT.fetchData(datePeriod);
        //the
        assertNotNull(userData);
    }

    @Test(expected = RestCallException.class)
    public void shouldThrowRestCallException() {
        //given
        LocalDate start = LocalDate.of(2017, 8, 4);
        LocalDate end = LocalDate.now();
        DatePeriod datePeriod = DatePeriod.between(start, end);
        when(CUT.loadData(datePeriod))
                .thenThrow(new RestCallException("invalid parameters", any()));
        //when
        CUT.loadData(datePeriod);
        //then exception
    }
}