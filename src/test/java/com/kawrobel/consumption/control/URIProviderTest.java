package com.kawrobel.consumption.control;

import com.kawrobel.consumption.control.utils.URIProvider;
import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.exceptions.URIParsingException;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class URIProviderTest {

    private static final String url = "http://finestmedia.ee/kwh/";
    private int startYear = 2016;
    private int startMonth = 6;
    private int startDay = 1;
    private int endYear = 2017;
    private int endMonth = 5;
    private int endDay = 2;

    @Test
    public void shouldReturnProperURI() throws URISyntaxException {
        URI result = URIProvider
                .provideURI(url, DatePeriod.between(LocalDate.of(startYear, startMonth, startDay),
                        LocalDate.of(endYear, endMonth, endDay)));
        URI expected = new URI("http://finestmedia.ee/kwh/?start=01-06-2016&end=02-05-2017");
        assertEquals("URIs should match", expected, result);
    }

    @Test(expected = URIParsingException.class)
    public void shouldThrowExpectedException() {
        URIProvider.provideURI(url + "^", DatePeriod.between(LocalDate.of(startYear, startMonth, startDay), LocalDate.of(endYear, endMonth, endDay)));
    }

    @Test(expected = RuntimeException.class)
    public void shouldThrowRuntimeExceptionIfWrongURI() {
        URIProvider.validate("htt://finestmedia.ee/kwh");
    }

    @Test
    public void shouldSuccessfullyValidateURI() {
        URIProvider.validate(url);
    }

}