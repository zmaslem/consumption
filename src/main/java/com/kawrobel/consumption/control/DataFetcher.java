package com.kawrobel.consumption.control;

import com.kawrobel.consumption.aop.annotations.TrackTime;
import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.entity.dto.UserData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.io.IOException;

/**
 * Abstract class responsible for getting data from cache, or calling the {@loadData} method
 * if cache does not provide us with requested data.
 * Any class that extends this one should implement the loadData method
 */
@Slf4j
public abstract class DataFetcher {

    private static final String CACHE_NAME = "userData";

    /**
     * Method responsible for getting data from cache (or any kind of DataFetcher that we are
     * currently in use if cache does not have data for given datePeriod)
     *
     * @param datePeriod - time datePeriod for which we need to provide user data energy consumption
     * @return Optional of UserData. If no data provided, we should expect Optional.empty()
     */
    @TrackTime
    @Cacheable(value = CACHE_NAME, key = "#datePeriod")
    public UserData fetchData(final DatePeriod datePeriod) {
        return loadData(datePeriod);
    }

    /**
     * This method should be implemented by any subclass of DataFetcher. It's role is to provide UserData object
     * from required source
     *
     * @param datePeriod - time datePeriod for which we need to provide user data energy consumption
     * @return UserData object as a representation of specific user's energy use in given time datePeriod
     * @throws IOException
     */
    @CachePut(value = CACHE_NAME, key = "#datePeriod")
    public abstract UserData loadData(DatePeriod datePeriod);
}
