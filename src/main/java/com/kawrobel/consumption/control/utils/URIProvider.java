package com.kawrobel.consumption.control.utils;

import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.exceptions.URIParsingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Slf4j
public class URIProvider {
    /**
     * static method used for call url preparation
     *
     * @param base - base url
     * @param start - start date
     * @param end - end date
     * @return - String as a representation of the call url
     */

    /**
     * Format of the date used for date formatting
     */
    private static final String DATE_FORMAT = "dd-MM-yyyy";
    /**
     * START - prefix of the call arguments
     * END - suffix of the call arguments
     */
    private static final String START = "start";
    private static final String END = "end";

    public static URI provideURI(final String base, final DatePeriod period) {
        try {
            final URIBuilder uriBuilder = new URIBuilder(base);
            uriBuilder.addParameter(START, formatDate(period.getStart()));
            uriBuilder.addParameter(END, formatDate(period.getEnd()));
            return uriBuilder.build();
        } catch (final URISyntaxException e) {
            throw new URIParsingException("Error during parsing uri", e);
        }
    }

    private static String formatDate(final LocalDate start) {
        return DateTimeFormatter.ofPattern(DATE_FORMAT, Locale.ENGLISH).format(start);
    }

    public static void validate(final String dataProvidingEndpoint) {
        try {
            new URL(dataProvidingEndpoint);
        } catch (Exception e1) {
            throw new RuntimeException("Invalid parameter: consumption.url", e1);
        }
    }
}
