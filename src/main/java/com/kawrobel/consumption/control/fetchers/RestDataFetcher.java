package com.kawrobel.consumption.control.fetchers;

import com.kawrobel.consumption.control.DataFetcher;
import com.kawrobel.consumption.control.utils.URIProvider;
import com.kawrobel.consumption.entity.converter.EnergyReportToUserDataConverter;
import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.entity.dto.UserData;
import com.kawrobel.consumption.entity.report.EnergyReport;
import com.kawrobel.consumption.exceptions.RestCallException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

/**
 * RestDataFetcher is an implementation of DataFetcher used for making a rest call for date
 */
@Component
@Slf4j
public class RestDataFetcher extends DataFetcher {

    private String endpoint;
    private RestTemplate restTemplate;

    public RestDataFetcher(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Value("${consumption.url}")
    private void setDataProvidingEndpoint(final String dataProvidingEndpoint) {
        URIProvider.validate(dataProvidingEndpoint);
        this.endpoint = dataProvidingEndpoint;
    }

    @Override
    public UserData loadData(final DatePeriod datePeriod) {
        final URI restEndpoint = URIProvider.provideURI(endpoint, datePeriod);
        final EnergyReport energyReport = askRestEndpointForData(restEndpoint);
        return EnergyReportToUserDataConverter.convertFromEnergyReport(energyReport, datePeriod);
    }

    EnergyReport askRestEndpointForData(final URI restEndpoint) {
        EnergyReport energyReport;
        try {
            energyReport = restTemplate.getForObject(restEndpoint, EnergyReport.class);
        } catch (final RestClientException exception) {
            log.debug("Problem with reaching data on :" + restEndpoint);
            throw new RestCallException("Problem reaching data source: " + exception.getMessage(), exception);
        }
        return energyReport;
    }
}
