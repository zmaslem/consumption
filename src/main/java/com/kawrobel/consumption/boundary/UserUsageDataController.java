package com.kawrobel.consumption.boundary;

import com.kawrobel.consumption.control.DataFetcher;
import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.entity.dto.UserData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

/**
 * Main RestController used for retrieving user energy usage data
 */
@RestController
@RequestMapping("/report")
@Slf4j
public class UserUsageDataController {

    private DataFetcher dataFetcher;

    private static final String FORMAT = "MM-dd-yyyy";

    public UserUsageDataController(final DataFetcher restDataFetcher) {
        this.dataFetcher = restDataFetcher;
    }

    /**
     * Method used for retrieving user usage date for given range of dates
     *
     * @param start - starting date
     * @param end   - end date
     * @return - ResponseEntity with UserData object containing consumption data for given range of dates in it
     */
    @GetMapping(value = "/{start}/{end}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fetchUserUsageDataForDates(@PathVariable("start") @DateTimeFormat(pattern = FORMAT) final LocalDate start,
                                                     @PathVariable("end") @DateTimeFormat(pattern = FORMAT) final LocalDate end) {
        final UserData userData = dataFetcher.fetchData(DatePeriod.between(start, end));
        return ResponseEntity.ok(userData);
    }
}
