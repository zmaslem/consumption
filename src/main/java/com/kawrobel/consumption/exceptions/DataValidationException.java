package com.kawrobel.consumption.exceptions;

public class DataValidationException extends RuntimeException {
    public DataValidationException(final String message) {
        super(message);
    }
}
