package com.kawrobel.consumption.exceptions;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class BaseExceptionHandler {

    @ExceptionHandler(DataValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleDataValidationException(final DataValidationException ex) {
        log.error(ex.getMessage(), ex);
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    @ExceptionHandler(NoDataAvailableException.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    ErrorResponse handleNoDataAvailableException(final NoDataAvailableException ex) {
        log.error(ex.getMessage(), ex);
        return new ErrorResponse(HttpStatus.NO_CONTENT.value(), ex.getMessage());
    }

    @ExceptionHandler(RestCallException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleRestCallException(final RestCallException ex) {
        log.error(ex.getMessage(), ex);
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    @ExceptionHandler(URIParsingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handlURIParsingException(final URIParsingException ex) {
        log.error(ex.getMessage(), ex);
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    @Data
    private static class ErrorResponse {
        private final int code;
        private final String message;
    }
}
