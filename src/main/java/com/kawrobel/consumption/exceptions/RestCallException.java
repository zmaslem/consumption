package com.kawrobel.consumption.exceptions;

public class RestCallException extends RuntimeException {
    public RestCallException(final String message, final Exception exception) {
        super(message, exception);
    }
}
