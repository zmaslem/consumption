package com.kawrobel.consumption.exceptions;

public class URIParsingException extends RuntimeException {
    public URIParsingException(final String errorDuringParsingUri, final Exception e) {
        super(errorDuringParsingUri, e);
    }
}
