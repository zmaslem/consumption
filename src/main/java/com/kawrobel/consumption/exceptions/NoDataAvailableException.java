package com.kawrobel.consumption.exceptions;

public class NoDataAvailableException extends RuntimeException {
    public NoDataAvailableException(final String message) {
        super(message);
    }

    public NoDataAvailableException(final String message, final Exception exception) {
        super(message, exception);
    }
}
