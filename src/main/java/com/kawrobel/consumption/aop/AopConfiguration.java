package com.kawrobel.consumption.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class AopConfiguration {

    /**
     * Method used for calculating invocation time of a method annotated with @TrackTime annotation.
     * Notice the TRACE level of logging
     *
     * @throws Throwable
     */
    @Around("@annotation(com.kawrobel.consumption.aop.annotations.TrackTime)")
    public Object trackTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        final Object proceed = joinPoint.proceed();
        long timeTaken = System.currentTimeMillis() - startTime;
        log.trace("Time Taken by {} is {}", joinPoint, timeTaken);
        return proceed;
    }
}
