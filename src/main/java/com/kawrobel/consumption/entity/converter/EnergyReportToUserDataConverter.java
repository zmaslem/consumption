package com.kawrobel.consumption.entity.converter;

import com.kawrobel.consumption.entity.dto.DatePeriod;
import com.kawrobel.consumption.entity.dto.Usage;
import com.kawrobel.consumption.entity.dto.UserData;
import com.kawrobel.consumption.entity.report.ConsumptionHistory;
import com.kawrobel.consumption.entity.report.EnergyReport;
import com.kawrobel.consumption.entity.report.HourConsumption;
import com.kawrobel.consumption.exceptions.NoDataAvailableException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EnergyReportToUserDataConverter {

    public static UserData convertFromEnergyReport(final EnergyReport energyReport, final DatePeriod datePeriod) {
        final List<Usage> dayConsumption =
                getConsumption(energyReport.getAccountTimeSeries().getConsumptionHistory(), datePeriod);
        BigDecimal total = getTotalConsumption(dayConsumption);
        return new UserData(dayConsumption, datePeriod, total);
    }

    private static BigDecimal getTotalConsumption(final List<Usage> consumptionHistory) {
        return consumptionHistory
                .stream()
                .map(entry -> entry.getConsumption()).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private static List<Usage> getConsumption(final ConsumptionHistory consumptionHistory, final DatePeriod datePeriod) {
        Map<LocalDate, List<HourConsumption>> map = consumptionHistory.getHourConsumption().stream()
                .collect(Collectors.groupingBy(e -> e.getTs().toLocalDate()));
        List<Usage> usage = filterRelevantOnlyAndGroupByDate(map, datePeriod);
        if (usage.isEmpty() || usage.size() == 0) {
            throw new NoDataAvailableException("No data available for provided dates");
        }
        return usage;
    }

    /**
     * This method takes map of date-listOfHourConsumption, first filter them to meet the start-end criteria,
     * next group them by day in a way to keep sum of consumption per day
     */
    private static List<Usage> filterRelevantOnlyAndGroupByDate(final Map<LocalDate, List<HourConsumption>> map,
                                                                final DatePeriod datePeriod) {
        return map.entrySet().stream()
                .filter(e -> e.getKey().compareTo(datePeriod.getStart()) >= 0 && e.getKey().compareTo(datePeriod.getEnd()) <= 0)
                .map(entry ->
                        new Usage(entry.getKey(), entry.getValue()
                                .stream()
                                .map(i -> i.getValue())
                                .reduce(BigDecimal.ZERO, BigDecimal::add)))
                .collect(Collectors.toList());
    }
}
