package com.kawrobel.consumption.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
public class UserData {
    private List<Usage> dayConsumption;
    private DatePeriod datePeriod;
    private BigDecimal total;
}
