package com.kawrobel.consumption.entity.dto;

import com.kawrobel.consumption.exceptions.DataValidationException;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@EqualsAndHashCode
public class DatePeriod {
    private LocalDate start;
    private LocalDate end;

    private DatePeriod() {
    }

    public DatePeriod(final LocalDate start, final LocalDate end) {
        validate(start, end);
        this.start = start;
        this.end = end;
    }

    public static DatePeriod between(final LocalDate start, final LocalDate end) {
        return new DatePeriod(start, end);
    }

    private void validate(final LocalDate start, final LocalDate end) {
        if (null == start || null == end || start.isAfter(end) || start.equals(end)) {
            throw new DataValidationException("Start or end dates are not valid");
        }
    }
}
