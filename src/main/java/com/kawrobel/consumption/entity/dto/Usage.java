package com.kawrobel.consumption.entity.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class Usage {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDate date;
    private BigDecimal consumption;
}
