package com.kawrobel.consumption.entity.report;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.util.List;

@Data
public class ConsumptionHistory {
    @JacksonXmlProperty(localName = "HourConsumption")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<HourConsumption> hourConsumption;
}
