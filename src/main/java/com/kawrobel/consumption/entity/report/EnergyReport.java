package com.kawrobel.consumption.entity.report;

/**
 * Representation of EnergyReport that is retrieved from endpoint in xml form
 */

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.time.ZonedDateTime;

@Data
@JacksonXmlRootElement
public class EnergyReport {
    @JacksonXmlElementWrapper(localName = "DocumentIdentification")
    private String documentIdentification;
    @JacksonXmlElementWrapper(localName = "DocumentDateTime")
    private ZonedDateTime documentDateTime;
    @JacksonXmlElementWrapper(localName = "AccountTimeSeries")
    private AccountTimeSeries accountTimeSeries;

}
