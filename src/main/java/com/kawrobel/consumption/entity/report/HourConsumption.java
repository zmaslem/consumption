package com.kawrobel.consumption.entity.report;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
public class HourConsumption {
    @JacksonXmlProperty
    private ZonedDateTime ts;
    @JacksonXmlText
    private BigDecimal value;
}
