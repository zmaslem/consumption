package com.kawrobel.consumption.entity.report;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import lombok.Data;

@Data
public class AccountTimeSeries {
    @JacksonXmlElementWrapper(localName = "MeasurementUnit")
    private MeasurementUnit measurementUnit;
    @JacksonXmlElementWrapper(localName = "AccountingPoint")
    private String accountingPoint;
    @JacksonXmlElementWrapper(localName = "ConsumptionHistory")
    private ConsumptionHistory consumptionHistory;
}
